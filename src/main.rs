extern crate dirs;
extern crate rash;

use rash::Rash;


fn main() {
    if let Ok(path) = std::env::current_exe() {
        std::env::set_var("SHELL", path);
    }
    let home = dirs::home_dir();

    let mut rash = Rash::new(&home);

    if let Some(ref dir) = home {
        rash.load_history(dir.join(".rash_history")).ok();
        rash.load_rc();
    }

    rash.repl();

    if let Some(ref dir) = home { rash.save_history(dir.join(".rash_history")).ok(); }
}
