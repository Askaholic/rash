use super::Rash;

mod alias;
mod cd;

use self::cd::cd;
use self::alias::{alias, unalias};
use super::Rule;

use std::collections::HashMap;
use std::fs;


pub enum Builtin<'a> {
    Exit,
    Cd(&'a[&'a str]),
    Alias(&'a[&'a str]),
    Unalias(&'a[&'a str]),
    Source(&'a[&'a str])
}

impl<'a> Builtin<'a> {
    pub fn from_str(cmd: &'a str, args: &'a[&'a str]) -> Option<Builtin<'a>> {
        use self::Builtin::*;
        match cmd {
            "exit" => Some(Exit),
            "cd" => Some(Cd(args)),
            "alias" => Some(Alias(args)),
            "unalias" => Some(Unalias(args)),
            "source" => Some(Source(args)),
            _ => None
        }
    }

    pub fn execute(&self, rash: &mut Rash) -> bool {
        use self::Builtin::*;
        match match self {
                Exit => (exit(), "exit"),
                Cd(_) => (cd(&self, &rash), "cd"),
                Alias(_) => (alias(&self, rash), "alias"),
                Unalias(_) => (unalias(&self, rash), "unalias"),
                Source(_) => (source(&self, rash), "source")
            } {
            (Ok(do_exit), _) => do_exit,
            (Err(e), cmd) => {
                println!("rash: {}: {}", cmd, e.msg);
                false
            }
        }
    }
}

pub struct BuiltinError {
    msg: String
}
impl BuiltinError {
    pub fn new(msg: String) -> BuiltinError {
        BuiltinError { msg }
    }

    pub fn new_result<T>(msg: &str) -> Result<T, BuiltinError> {
        Err(Self::new(String::from(msg)))
    }
}
impl From<std::io::Error> for BuiltinError {
    fn from(err: std::io::Error) -> BuiltinError {
        BuiltinError::new(format!("{}", err))
    }
}
impl From<pest::error::Error<Rule>> for BuiltinError {
    fn from(err: pest::error::Error<Rule>) -> BuiltinError {
        BuiltinError::new(format!("syntax error {}", err))
    }
}

fn exit() -> Result<bool, BuiltinError> {
    println!("exit");
    Ok(true)
}

fn source(cmd: &Builtin, rash: &mut Rash) -> Result<bool, BuiltinError> {
    let args = if let Builtin::Source(args) = cmd { args } else { panic!("Not a Source"); };

    if args.len() != 1 {
        println!("source: usage: source filename [arguments]");
        return Ok(false);
    }
    let program = fs::read_to_string(&args[0])?;
    let program = rash.parse_program(&program, HashMap::new())?;

    rash.execute_program(program);
    Ok(false)
}
