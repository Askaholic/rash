use super::{BuiltinError, Builtin};
use crate::Rash;

use std::path::PathBuf;

pub fn cd(cmd: &Builtin, rash: &Rash) -> Result<bool, BuiltinError> {
    let args = if let Builtin::Cd(args) = cmd { args } else { panic!("Not a CD"); };

    if args.len() > 1 {
        return BuiltinError::new_result("too many arguments");
    }

    let mut cwd = std::env::current_dir()?;
    let to_dir = if args.len() == 0 {
        rash.home.clone().unwrap_or(cwd.clone())
    }
    else {
        PathBuf::from(args[0])
    };
    let dir_str = to_dir.to_string_lossy();
    if let "-" = &*dir_str {
        let oldpwd = std::env::var("OLDPWD")
            .or(BuiltinError::new_result("OLDPWD not set"))?;
        set_current_dir(&PathBuf::from(oldpwd))?;
        return Ok(false);
    }
    if dir_str.starts_with("/") { cwd = to_dir; } else { cwd.push(to_dir); }
    let path = cwd.canonicalize()?;
    set_current_dir(&path)?;
    Ok(false)
}

pub fn set_current_dir(path: &PathBuf) -> Result<bool, BuiltinError> {
    let pwd = std::env::var("PWD").unwrap_or(String::new());
    std::env::set_current_dir(&path)?;
    std::env::set_var("PWD", path);
    std::env::set_var("OLDPWD", pwd);
    Ok(false)
}
