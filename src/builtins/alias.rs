use super::{BuiltinError, Builtin};
use crate::Rash;
use regex::Regex;

pub fn alias(cmd: &Builtin, rash: &mut Rash) -> Result<bool, BuiltinError> {
    lazy_static! {
        static ref RE: Regex = Regex::new("^([a-zA-Z0-9_-]+)=(?:(.*))$").unwrap();
    }
    let args = if let Builtin::Alias(args) = cmd { args } else { panic!("Not an Alias"); };
    if args.len() == 0 {
        let mut aliases: Vec<(&String, &String)> = rash.aliases.iter().collect();
        aliases.sort_unstable();
        for (alias, target) in aliases {
            println!("alias {}='{}'", alias, target);
        }
        return Ok(false);
    }

    for arg in args.iter() {
        if RE.is_match(arg) {
            let caps = RE.captures(arg).unwrap();
            let alias = String::from(&caps[1]);
            let target = String::from(&caps[2]);

            rash.aliases.insert(alias, target);
        }
        else {
            if let Some(t) = rash.aliases.get(*arg) {
                println!("alias {}='{}'", arg, t);
            };
        }
    }

    Ok(false)
}

pub fn unalias(cmd: &Builtin, rash: &mut Rash) -> Result<bool, BuiltinError> {
    let args = if let Builtin::Unalias(args) = cmd { args } else { panic!("Not an Unalias"); };

    if args.len() == 0 {
        println!("unalias: usage: unalias [-a] name [name ...]");
        return Ok(false);
    }
    if args[0] == "-a" {
        rash.aliases.clear();
        return Ok(false);
    }
    for arg in args.iter() {
        if rash.aliases.contains_key(*arg) {
            rash.aliases.remove(*arg);
        }
        else {
            println!("unalias: {}: not found", arg);
        }
    }

    Ok(false)
}
