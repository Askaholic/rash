#![feature(if_while_or_patterns)]

extern crate pest;
extern crate rustyline;
extern crate regex;
#[macro_use] extern crate pest_derive;
#[macro_use] extern crate lazy_static;

pub mod builtins;
mod grammar;

use pest::Parser;
use rustyline::{Helper, Config, CompletionType};
use rustyline::error::ReadlineError;
use rustyline::completion::{FilenameCompleter, Completer, Pair};
use rustyline::hint::Hinter;
use rustyline::highlight::Highlighter;
use regex::Regex;
use std::collections::HashMap;
use std::path::{PathBuf};
use std::process;
use std::env;
use std::io::ErrorKind;
use std::borrow::Cow::{self, Borrowed, Owned};

use self::builtins::Builtin;
use self::grammar::*;

struct RashHelper(FilenameCompleter);
impl Completer for RashHelper {
    type Candidate = Pair;

    fn complete(&self, line: &str, pos: usize) -> Result<(usize, Vec<Pair>), ReadlineError> {
        self.0.complete(line, pos)
    }
}
impl Hinter for RashHelper {
    fn hint(&self, line: &str, _pos: usize) -> Option<String> {
        None
    }
}
impl Highlighter for RashHelper {
    fn highlight_prompt<'p>(&self, prompt: &'p str) -> Cow<'p, str> {
        Borrowed(prompt)
    }

    fn highlight_hint<'h>(&self, hint: &'h str) -> Cow<'h, str> {
        Borrowed(hint)
    }

    fn highlight<'l>(&self, line: &'l str, pos: usize) -> Cow<'l, str> {
        Borrowed(line)
    }

    fn highlight_char<'l>(&self, line: &'l str) -> bool {
        false
    }
}
impl Helper for RashHelper {}

pub struct Rash {
    home: Option<PathBuf>,
    editor: rustyline::Editor<RashHelper>,
    aliases: HashMap<String, String>
}

impl Rash {
    pub fn new(home: &Option<PathBuf>) -> Rash {
        let config = Config::builder()
               .completion_type(CompletionType::List)
        .build();
        let mut editor = rustyline::Editor::<RashHelper>::with_config(config);
        editor.set_helper(Some(RashHelper(FilenameCompleter::new())));
        Rash { home: home.clone(), editor, aliases: HashMap::new() }
    }

    pub fn load_history(&mut self, path: PathBuf) -> rustyline::Result<()> {
        self.editor.load_history(&path)
    }

    pub fn save_history(&self, path: PathBuf) -> rustyline::Result<()>  {
        self.editor.save_history(&path)
    }

    pub fn load_rc(&mut self) {
        let path = self.expand_home("~/.rashrc");
        let args = &[path.as_str()];
        Builtin::Source(args).execute(self);
    }

    pub fn repl(&mut self) {
        loop {
            match self.editor.readline(&self.get_command_prefix()) {
                Ok(input) => {
                    let command = input.trim().to_string();
                    let program = match self.parse_program(&command, HashMap::new()) {
                        Ok(prg) => prg,
                        Err(err) => {
                            println!("rash: syntax error{}", err);
                            continue;
                        }
                    };

                    self.editor.add_history_entry(command);
                    if self.execute_program(program) { return; };
                },
                Err(ReadlineError::Eof) => return,
                Err(ReadlineError::Interrupted) => println!("^C"),
                Err(err) => println!("rash: rustyline: {}", err)
            }
        }
    }

    fn get_command_prefix(&self) -> String {
        let cwd = get_cwd();
        let cwd = cwd.to_string_lossy();
        let cwd = self.shorten_home(&cwd);
        format!("\x1b[01;36mrash\x1b[00m:\x1b[01;34m{}\x1b[00m$ ", cwd)
    }

    fn parse_program(&self, cmd_str: &str, mut expanded_aliases: HashMap<String, bool>) -> Result<RashProgram, pest::error::Error<Rule>> {
        let program = RashParser::parse(Rule::program, cmd_str)?.next().unwrap();
        let mut rash_program = RashProgram::new();

        for stmt in program.into_inner() {
            if let Rule::EOI = stmt.as_rule() { continue; }

            let statements = self.parse_statement(stmt, &mut expanded_aliases)?;
            for stmt in statements {
                rash_program.add_statement(stmt);
            }
        }

        Ok(rash_program)
    }

    fn parse_statement(
        &self,
        executable: pest::iterators::Pair<'_, Rule>,
        expanded_aliases: &mut HashMap<String, bool>
    ) -> Result<Vec<RashStatement>, pest::error::Error<Rule>> {
        let mut inner = executable.into_inner();
        let path = inner.next().expect("No path found");
        let expanded_paths = self.expand_regex_paths(path)?;
        let args = inner.next().expect("No args found");
        let mut statements: Vec<RashStatement> = Vec::with_capacity(expanded_paths.len());
        for path in expanded_paths.into_iter() {
            statements.append(
                &mut self.parse_concrete_executable(path, args.clone(), expanded_aliases)?
            );
        }
        Ok(statements)
    }

    fn expand_regex_paths(&self, regex_path: pest::iterators::Pair<'_, Rule>) -> Result<Vec<String>, pest::error::Error<Rule>> {
        let mut expanded: Vec<String> = vec![String::new()];
        for comp in regex_path.into_inner() {
            match comp.as_rule() {
                Rule::path_section => expanded.iter_mut().for_each(|p| p.push_str(comp.as_str())),
                Rule::regex_literal => {
                    let regex = comp.into_inner().next().expect("Missing inner regex while expanding path");
                    let regex = unwrap_regex(Regex::new(&format!("^{}$", regex.as_str())), regex)?;
                    let mut new_segments = vec![];
                    for dir in expanded.iter() {
                        let path = if dir.is_empty() { "." } else { dir };
                        match std::fs::read_dir(path) {
                            Ok(dir_iter) => for dir_entry in dir_iter {
                                if let Ok(dir_entry) = dir_entry {
                                    if let Some(file_name) = dir_entry.file_name().to_str() {
                                        if regex.is_match(file_name) {
                                            new_segments.push(dir_entry.path().to_str().unwrap().to_string());
                                        }
                                    }
                                }
                            },
                            Err(_) => continue
                        }
                    }
                    expanded.clear();
                    expanded.append(&mut new_segments);
                },
                r => panic!("Unexpected rule type found in path: {:?}", r)
            }
        }
        Ok(expanded.iter().map(|p| self.expand_home(p)).collect())
    }

    fn parse_concrete_executable(
        &self, path: String,
        args: pest::iterators::Pair<'_, Rule>,
        expanded_aliases: &mut HashMap<String, bool>
    ) -> Result<Vec<RashStatement>, pest::error::Error<Rule>> {
        let args_str = args.as_str();
        let args_list = self.parse_args(args);
        let mut statements: Vec<RashStatement> =
            args_list.into_iter()
                .map(|args| RashStatement::new(path.clone(), args))
                .collect();
        let statement = &statements[0];

        if let (None, Some(_)) = (expanded_aliases.get(statement.path()), self.aliases.get(statement.path())) {
            let new_cmd = self.replace_alias(statement.path(), expanded_aliases);

            let new_cmd = format!("{} {}", new_cmd, args_str);
            statements = self.parse_statement(
                RashParser::parse(Rule::executable, &new_cmd)?.next().unwrap(),
                expanded_aliases
            )?;
        }
        Ok(statements)
    }

    fn parse_args(&self, args: pest::iterators::Pair<'_, Rule>) -> Vec<Vec<String>> {
        args.into_inner().map(|arg| self.parse_arg(arg)).collect()
    }

    fn parse_arg(&self, arg: pest::iterators::Pair<'_, Rule>) -> Vec<String> {
        vec![arg.into_inner().map(|token| {
            match token.as_rule() {
                Rule::path => self.expand_home(token.as_str()),
                Rule::single_string | Rule::double_string => token.into_inner().as_str().to_string(),
                _ => token.as_str().to_string()
            }
        }).collect()]
    }

    fn execute_program(&mut self, program: RashProgram) -> bool {
        for statement in program.into_statements() {
            if let true = self.execute_statement(statement) {
                return true;
            }
        }

        false
    }

    fn execute_statement(&mut self, statement: RashStatement) -> bool {
        let path = statement.path();
        let args_ref: Vec<&str> = statement.args().map(|s| s.as_str()).collect();

        if let Some(b) = Builtin::from_str(path, &args_ref) {
            return b.execute(self);
        }
        match process::Command::new(path)
                    .args(&args_ref)
                    .spawn()
        {
            Ok(mut child) => {
                let error_code = child.wait().expect("Failed to wait for child");
            },
            Err(ref e) if e.kind() == ErrorKind::NotFound => println!("{}: command not found", path),
            Err(e) => println!("{}", e)
        }
        false
    }

    /// Replaces `path` with the alias for `path`
    /// # Panics
    /// If `path` does not define a valid alias
    fn replace_alias(&self, path: &str, expanded: &mut HashMap<String, bool>) -> String {
        let target = self.aliases.get(path).expect("alias does not exist");

        if let Some(true) = expanded.get(target) { return String::from(path); }

        expanded.insert(String::from(path), true);
        target.trim_end().to_string()
    }

    fn shorten_home(&self, path: &str) -> String {
        match &self.home {
            Some(home_path) => path.replacen(&*home_path.to_string_lossy(), "~", 1),
            None => String::from(path)
        }
    }

    fn expand_home(&self, path: &str) -> String {
        match &self.home {
            Some(home_path) => path.replacen("~", &*home_path.to_string_lossy(), 1),
            None => String::from(path)
        }
    }
}

fn get_cwd() -> PathBuf {
    env::current_dir().unwrap_or(PathBuf::new())
}

fn unwrap_regex(re: Result<Regex, regex::Error>, regex_token: pest::iterators::Pair<'_, Rule>) -> Result<Regex, pest::error::Error<Rule>> {
    match re {
        Ok(r) => Ok(r),
        Err(e) => {
            let message = scrape_regex_error(e);
            let span = regex_token.as_span();

            Err(pest::error::Error::new_from_pos(
                pest::error::ErrorVariant::CustomError {
                    message
                },
                span.start_pos()
            ))
        }
    }
}

fn scrape_regex_error(e: regex::Error) -> String {
    use regex::Error::*;
    match e {
        Syntax(msg) => {
            lazy_static! {
                static ref RE: Regex = Regex::new(r"(?m)\A(.*)\n^    .*\n^    ([ ]*)\^*\n^error: (.*)\z").unwrap();
            }
            if ! RE.is_match(&msg) {
                return format!("regex parse error: {}", msg);
            }

            let caps = RE.captures(&msg).unwrap();
            let regex_parse_error = String::from(&caps[1]);
            // RIP the dream...
            // If pest let you change Positions, then we could use this to correct the
            // cursor offset in the displayed regex error.
            let _offset = &caps[2];
            let error_msg = String::from(&caps[3]);

            format!("{} {}", regex_parse_error, error_msg)
        },
        CompiledTooBig(_) => e.to_string(),
        _ => unreachable!()
    }
}
