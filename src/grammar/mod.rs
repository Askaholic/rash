#[derive(Parser)]
#[grammar = "grammar/rash.pest"]
pub struct RashParser;

#[derive(Debug)]
pub struct RashProgram {
    statements: Vec<RashStatement>
}
impl RashProgram {
    pub fn new() -> RashProgram {
        RashProgram { statements: vec![] }
    }

    pub fn add_statement(&mut self, stmt: RashStatement) {
        self.statements.push(stmt);
    }

    pub fn statements(&self) -> &Vec<RashStatement> {
        &self.statements
    }

    pub fn into_statements(self) -> Vec<RashStatement> {
        self.statements
    }
}
#[derive(Debug)]
pub struct RashStatement {
    path: String,
    args: Vec<String>
}
impl RashStatement {
    pub fn new(path: String, args: Vec<String>) -> RashStatement {
        RashStatement { path, args }
    }

    pub fn path(&self) -> &str {
        self.path.as_str()
    }

    pub fn args(&self) -> std::slice::Iter<'_, String> {
        self.args.iter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pest::Parser;

    #[test] fn parse_comment() {
        RashParser::parse(Rule::COMMENT, "# This is a comment\n").unwrap();
        RashParser::parse(Rule::COMMENT, "# This is a comment").unwrap();
        RashParser::parse(Rule::COMMENT, "This is a not comment").unwrap_err();
    }

    #[test] fn parse_arg() {
        RashParser::parse(Rule::argument, "some").unwrap();
        RashParser::parse(Rule::argument, "'string arg'").unwrap();
    }

    #[test] fn parse_executable() {
        RashParser::parse(Rule::executable, "ls").unwrap();
        RashParser::parse(Rule::executable, "ls -a").unwrap();
    }

    #[test] fn parse_program_1() {
        let mut programs = RashParser::parse(Rule::program, "ls\n").expect("Failed to parse");
        let prog = programs.next().expect("Missing program");
        let mut inner = prog.into_inner();
        inner.next().expect("Missing executable");
        inner.next().expect("Missing EOL");
        assert_eq!(inner.next(), None);
        assert_eq!(programs.next(), None);
    }

    #[test] fn parse_program_2() {
        let parse_result = RashParser::parse(Rule::program, "ls\nls");
        if let Err(e) = parse_result .clone(){
            println!("{}", e);
        }
        let mut programs = parse_result.expect("Failed to parse");
        let prog = programs.next().expect("Missing program");
        let mut inner = prog.into_inner();
        println!("{:#?}", inner);
        assert_eq!(inner.next().expect("Missing executable").as_rule(), Rule::statement);
        assert_eq!(inner.next().expect("Missing executable 2").as_rule(), Rule::statement);
        assert_eq!(inner.next().expect("Missing EOI").as_rule(), Rule::EOI);
        assert_eq!(inner.next(), None);
        assert_eq!(programs.next(), None);
    }

    #[test] fn parse_program_3() {
        let parse_result = RashParser::parse(Rule::program, "ls -a\nalias ls='ls --color=auto'");
        if let Err(e) = parse_result .clone(){
            println!("{}", e);
        }
        let mut programs = parse_result.expect("Failed to parse");
        let prog = programs.next().expect("Missing program");
        let mut inner = prog.into_inner();
        println!("{:#?}", inner);
        let exe1 = inner.next().expect("Missing executable");
        assert_eq!(exe1.as_rule(), Rule::statement);
        let mut exe1_inner = exe1.into_inner();
        assert_eq!(exe1_inner.next().expect("Missing path").as_rule(), Rule::path);
        assert_eq!(exe1_inner.next().expect("Missing args").as_str(), "-a");
        let exe2 = inner.next().expect("Missing executable 2");
        assert_eq!(exe2.as_rule(), Rule::statement);
        let mut exe2_inner = exe2.into_inner();
        assert_eq!(exe2_inner.next().expect("Missing path2").as_rule(), Rule::path);
        assert_eq!(exe2_inner.next().expect("Missing args2").as_str(), "ls='ls --color=auto'");
        assert_eq!(inner.next().expect("Missing EOI").as_rule(), Rule::EOI);
        assert_eq!(inner.next(), None);
        assert_eq!(programs.next(), None);
    }

    #[test] fn parse_program_4() {
        let parse_result = RashParser::parse(Rule::program, "# comment\nls");
        if let Err(e) = parse_result .clone(){
            println!("{}", e);
        }
        let mut programs = parse_result.expect("Failed to parse");
        let prog = programs.next().expect("Missing program");
        let mut inner = prog.into_inner();
        println!("{:#?}", inner);
        inner.next().expect("Missing executable");
        inner.next().expect("Missing EOL");
        assert_eq!(inner.next(), None);
        assert_eq!(programs.next(), None);
    }

    #[test] fn parse_program_5() {
        let parse_result = RashParser::parse(Rule::program, "# comment\nls -al -f");
        if let Err(e) = parse_result .clone(){
            println!("{}", e);
        }
        let mut programs = parse_result.expect("Failed to parse");
        let prog = programs.next().expect("Missing program");
        let mut inner = prog.into_inner();
        println!("{:#?}", inner);
        let exe1 = inner.next().expect("Missing executable");
        assert_eq!(exe1.as_rule(), Rule::statement);
        let mut exe1_inner = exe1.into_inner();
        assert_eq!(exe1_inner.next().expect("Missing path").as_rule(), Rule::path);
        assert_eq!(exe1_inner.next().expect("Missing args").as_str(), "-al -f");
        inner.next().expect("Missing EOL");
        assert_eq!(inner.next(), None);
        assert_eq!(programs.next(), None);
    }

    macro_rules! assert_path_section {
        ($inner:ident, $literal:tt) => {
            let path_sec = $inner.next().expect("Missing path section");
            assert_eq!(path_sec.as_rule(), Rule::path_section);
            assert_eq!(path_sec.as_str(), $literal);
        };
    }
    macro_rules! assert_regex_literal {
        ($inner:ident, $literal:tt, $inner_literal:tt) => {
            let regex_literal = $inner.next().expect("Missing regex literal");
            assert_eq!(regex_literal.as_rule(), Rule::regex_literal);
            assert_eq!(regex_literal.as_str(), $literal);
            assert_regex!(regex_literal, $inner_literal);
        };
    }
    macro_rules! assert_regex {
        ($regex_literal:ident, $literal:tt) => {
            let mut inner = $regex_literal.into_inner();
            let regex = inner.next().expect("Missing regex");
            assert_eq!(regex.as_rule(), Rule::regex);
            assert_eq!(regex.as_str(), $literal);
            assert_eq!(inner.next(), None);
        };
    }

    #[test] fn parse_regex() {
        let parse_result = RashParser::parse(Rule::regex_literal, "{c+}");
        if let Err(e) = parse_result .clone(){
            println!("{}", e);
        }
        let mut regexs = parse_result.expect("Failed to parse");
        let regex = regexs.next().unwrap();
        println!("{:#?}", regex);
        assert_regex!(regex, "c+");
        assert_eq!(regexs.next(), None);
    }

    #[test] fn parse_regex_path() {
        let parse_result = RashParser::parse(Rule::path, "/{.*}/bin/{l+}/bin/ls");
        if let Err(e) = parse_result .clone(){
            println!("{}", e);
        }
        let mut paths = parse_result.expect("Failed to parse");
        let path = paths.next().unwrap();
        println!("{:#?}", path);
        let mut inner = path.into_inner();
        assert_path_section!(inner, "/");
        assert_regex_literal!(inner, "{.*}", ".*");
        assert_path_section!(inner, "/bin/");
        assert_regex_literal!(inner, "{l+}", "l+");
        assert_path_section!(inner, "/bin/ls");
        assert_eq!(inner.next(), None);
        assert_eq!(paths.next(), None);
    }
}
